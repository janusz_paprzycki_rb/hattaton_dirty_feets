var webpack = require('webpack');
var validate = require('webpack-validator');
var merge = require('webpack-merge');

var common = module.exports = {
    entry: [
        './client/client.js'
    ],
    output: {
        path: require("path").resolve("./dist"),
        filename: 'bundle.js',
        publicPath: '/'
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass']
            },
            {
                test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
                loader: "file"
            }
        ]
    }
};

var config;
switch(process.env.npm_lifecycle_event) {
    case 'build':
        config = merge.smart(common, require("./webpack-build"));
        break;
    default:
        config = merge.smart(common, require("./webpack-dev"));
}

console.log(process.env.npm_lifecycle_event);

module.exports = validate(config);
