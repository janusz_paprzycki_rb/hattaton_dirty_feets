import React, { Component } from 'react'
import { render } from 'react-dom'
import "../sass/main.scss"
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import LoginRegister from '../components/LoginRegister/LoginRegister';
import Treatments from '../components/Treatments/Treatments';
import Timeline from '../components/Timeline/Timeline';
import Reminder from '../components/Reminder/Reminder';
import Congratulations from '../components/Congratulations/Congratulations';


render(
	<div className="container">
    <Router history={browserHistory}>
        <Route path="/" component={LoginRegister} />
        <Route path="treatments" component={Treatments} />
        <Route path="timeline" component={Timeline} />
        <Route path="reminder" component={Reminder} />
        <Route path="congratulations" component={Congratulations} />
    </Router>
    </div>,
    document.getElementById("app")
);

