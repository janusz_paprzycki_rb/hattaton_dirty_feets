var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ReplacePlugin = require('replace-webpack-plugin');

module.exports = {
    devtool: 'cheap-module-source-map',
    output: {
        filename: 'bundle.min.js'
    },
    plugins: [
        new CopyWebpackPlugin([
            {from: './public', to: "./"}
        ]),
        new ReplacePlugin({
            entry: './client/index.html',
            output: './dist/index.html',
            data: {
                js: '<script src="bundle.min.js"></script>'
            }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            // Don't beautify output (enable for neater output)
            beautify: false,

            // Eliminate comments
            comments: false,
            compress: {
                warnings: false
            },
            mangle: {
                // Don't mangle $
                except: ['$'],

                // Don't care about IE8
                screw_ie8: true,

                // Don't mangle function names
                keep_fnames: true
            }
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['react', 'es2015']
                }
            }
        ]
    }
};
