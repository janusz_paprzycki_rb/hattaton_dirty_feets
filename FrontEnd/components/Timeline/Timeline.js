import React, {Component} from 'react';
import { browserHistory } from 'react-router';


class Timeline extends Component {

    navigateReminder() {
        browserHistory.push('/reminder');
    }

    render() {
        const treatment = JSON.parse(localStorage.getItem('treatment'));

        const startDate = treatment.startDate;
        const endDate = treatment.endDate;
        const days = treatment.days;
        const elapsedDays = treatment.elapsedDays;

        const progressBarLength = () => {
          return elapsedDays / days * 100;
        };

        return (
            <div onClick={this.navigateReminder}>
                <div className="jumbotron">
                    <h1>{treatment.product}</h1>
                    <div>{treatment.desc}</div>
                </div>

                <div className="countdown">Days left
                    <h1 className="countdown">{days - elapsedDays}</h1>
                </div>

                <div className="timeline clearfix">
                    <div className="progress">
                        <div className="progress-bar progress-bar-striped progress-bar-success" style={{'width': progressBarLength() + "%"}}></div>
                        <div className="progress-bar progress-bar-striped progress-bar-danger" style={{'width': (100 - progressBarLength()) + "%"}}></div>
                    </div>
                    <div className="star-end-date pull-left">{startDate}</div>
                    <div className="start-end-date pull-right">{endDate}</div>
                </div>
            </div>
        )
    }
}

export default Timeline;