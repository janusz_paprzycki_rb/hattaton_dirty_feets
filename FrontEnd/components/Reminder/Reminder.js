import React, { Component } from 'react';
import { browserHistory } from 'react-router';


class Reminder extends Component {

    handleDone() {
        browserHistory.push('/congratulations');
    }

    render() {
        const treatment = JSON.parse(localStorage.getItem('treatment'));
        
        return(
            <div className="reminder">
                <h2>Remember to take another dose of the {treatment.product} on</h2>
                <h1>{treatment.endDate}</h1>
                <button className="btn btn-lg btn-primary" onClick={this.handleDone}>Done</button>
            </div>
        )
    }

}


export default Reminder;