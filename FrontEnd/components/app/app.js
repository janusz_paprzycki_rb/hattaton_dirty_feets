import React, { Component } from 'react'
import LoginRegister from '../LoginRegister/LoginRegister';

class App extends Component {
    render() {
        return(
            <div className="container">
                <LoginRegister />
            </div>
        )
    }
}

export default App