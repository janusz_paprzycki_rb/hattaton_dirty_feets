import React, { Component } from 'react';

class Congratulations extends Component {
    render() {
        return(
            <div>
                <div className="jumbotron">
                    <h1>CONGRATULATIONS</h1>
                </div>
                <div className="promocode">
                    <p>You have only <span className="promocode">X</span> doses left. Remember to buy a new package.</p>

                    <p>You can but it <a className="btn btn-lg btn-link" href="http://www.scholl.co.uk/" target="blank">HERE</a></p>

                    <p>Use this code: <span className="label label-info">Xxxxxxx</span> to get a 20% discount at our store.</p>
                </div>
            </div>
        )
    }
}

export default Congratulations;