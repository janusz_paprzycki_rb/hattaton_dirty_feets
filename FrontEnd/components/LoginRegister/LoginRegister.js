import React, { Component } from 'react';
import LoginRegisterForm from './LoginRegisterForm';
import { browserHistory } from 'react-router';


class LoginRegister extends Component {

    navigateTreatments(user) {
        localStorage.setItem('user', JSON.stringify(user));
        browserHistory.push('/treatments')
    }

    submitLogin(user) {
        this.navigateTreatments(user);
    }

    submitRegister(user) {
        this.navigateTreatments(user);
    }

    render() {
        return(
            <div>
                <div className="jumbotron">
                    <h1>Scholl Aid</h1>
                </div>
                <div className="row login-register">
                    <LoginRegisterForm type="Login" submit={this.submitLogin.bind(this)} />
                    <div className="col-sm-2 login-register-or"><span className="label label-default">OR</span></div>
                    <LoginRegisterForm type="Register" submit={this.submitRegister.bind(this)} />
                </div>
            </div>
        )
    }

}


export default LoginRegister;