import React, { Component } from 'react';

class LoginRegisterForm extends Component {
    constructor() {
        super();

        this.state = {
            name: "",
            email: "",
            TCagree: false
        }
    }

    handleUserNameChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleEmailChange(e) {
        this.setState({
            email: e.target.value
        });
    }

    handleCheckboxChange() {
        const { TCagree }  = this.state;

        this.setState({TCagree: !TCagree});
    }

    handleSubmit(e) {
        e.preventDefault();

        this.props.submit(this.state)
    }

    getRegisterEmail() {
        return this.props.type === "Register" ? <input type="email" className="form-control" placeholder="E-mail...." required value={this.state.email} onChange={this.handleEmailChange.bind(this)} /> : "";
    }

    getRegisterTC() {
        return this.props.type === "Register" ? <div><input type="checkbox" required value={this.state.TCagree} onChange={this.handleCheckboxChange.bind(this)}/> I agree for <a href="#">Terms and Conditions</a></div> : "";
    }

    render() {
        return(
            <form className="col-sm-5" onSubmit={this.handleSubmit.bind(this)}>
                <input type="text" className="form-control" placeholder="User name....." required value={this.state.name} onChange={this.handleUserNameChange.bind(this)} />
                { this.getRegisterEmail() }
                { this.getRegisterTC() }
                <button type="submit" className="btn btn-lg btn-primary">{this.props.type}</button>
            </form>
        )
    }

}


export default LoginRegisterForm;

