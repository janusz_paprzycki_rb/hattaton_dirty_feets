import React, { Component } from 'react';
import { browserHistory } from 'react-router';


const treatments = [
    {name: "Fungal Nail", product: "Fungal Nail Treatment", time: "9 months", desc: "Scholl Fungal Nail Treatment has been specially designed to treat fungal nail with two systems: nail files and an advanced nail liquid.", startDate: "10.06.2016", endDate: "10.03.2017", days: 243, elapsedDays: 61},
    {name: "Athlete’s Foot", product: "Athlete’s Foot Complete Pen & Spray Kit", time: "4 weeks", desc: "A unique 2-step solution: The Pen and the Shoe Spray together not only treat athlete’s foot, but they also help restore & protect the skin to reduce the risk of infection reoccurring.", startDate: "05.08.2016", endDate: "08.08.2016", days: 28, elapsedDays: 5},
    {name: "Cracked Heel", product: "Cracked Heel Repair Cream K+ 120ml/60ml", time: "7 days", desc: "Moisturiser suitable for cracked heels. Unscented and quickly absorbed.", startDate: "05.08.2016", endDate: "12.08.2016", days: 7, elapsedDays: 5}
];

class Treatments extends Component {
    handleTreatmentClick(treatment) {
        localStorage.setItem('treatment', JSON.stringify(treatment));
        browserHistory.push('/timeline');
    }

    render() {
        const user = JSON.parse(localStorage.getItem('user'));

        const treatmentList = treatments.map((val, index) => <a className="list-group-item clearfix" key={index} onClick={this.handleTreatmentClick.bind(this, val)}>
            <span className="pull-left">{val.name}</span><span className="pull-right">{val.time}</span>
        </a>);

        return(
            <div>
                <div className="jumbotron clearfix">
                    <h2 className="pull-right text-muted">{user.name}</h2>
                    <h1 className="pull-left">Choose your treatment</h1>
                </div>
                <div className="list-group treatment-list">
                    {treatmentList}
                </div>
            </div>
        )
    }
}

export default Treatments;